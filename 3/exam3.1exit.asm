#PURPOSE: Simple Example Program that exits and return
#	  status code back to the Linux kernel
#

#INPUT:	none

#OUTPUT: Returns a status code. Can be viewed by typing:
#
#	echo $?
#
#	after running the program.

#VARIABLES:
#	%eax holds the system call number
#	%ebx holds the return status

.section .data

.section .text

.globl _start

_start:
	movl $1, %eax	#this is the linux kernel command
			#number for exiting a program
	movl $17, %ebx	#the status number to return to the OS
	int $0x80	#this interups the kernel to run the exit syscall
