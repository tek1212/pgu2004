#PURPOSE: Finds the maximum number of a set of data items

#VARIABLES: The registers have the following uses:
#	%edi - holds the index of the data item being examined
#	%ebx - largest data item found
#	%eax - current examined data item

# The following memory locations are used:
# data_items - contains the items data. A 0 is used to terminate.

.section .data
data_items:			#these are the data items
	.long 3,66,34,222,57,75,45

.section .text

.globl _start

_start:
	movl $0, %edi 		#initializes the index reg w. 0
	movl data_items(,%edi,4), %eax 	#load the 1st data
	movl %eax, %ebx		#1st biggest data item

start_loop:
	cmpl $0, %eax		#check to see if it's end
	je loop_exit
	incl %edi		#move index to next value
	movl data_items(,%edi,4), %eax
	cmpl %ebx, %eax		#compare values
	jle start_loop		#jump to start_loop if eax
				#isn't bigger than ebx
	movl %eax, %ebx		#store the new bigger value
	jmp start_loop		#jump to start_loop

loop_exit:			#the ebx already has max value to be returned
	movl $1, %eax		#load the exit() syscall
	int $0x80		#signal the interupt handler (kernel inthiscase)
